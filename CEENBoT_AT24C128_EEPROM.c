/*
 * Copyright (C) 2014 The Board of Regents of the University of Nebraska.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
 * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "CEENBoT_AT24C128_EEPROM.h"


volatile unsigned char curByteH;
volatile unsigned char curByteL;

unsigned char MemLocationL[2] = { 0x00, 0x01 };
unsigned char MemLocationH[2] = { 0x00, 0x00 };

/**
Function: CEENBoT_AT24C128_writes
Arguments:
	input	- Address of a C style string to be stored in EEPROM
	length	- length of string
Details: This takes the string passed in and stores it into EEPROM on the I2C bus
 */
void CEENBoT_AT24C128_writes(char *input, int length) {
	if (SYS_get_state(SUBSYS_I2C) == SUBSYS_OPEN) {
		unsigned int i;
		while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
			;
		I2C_MSTR_send_multiple(MemLocationH, 2);
		I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MR);
		I2C_MSTR_get(&curByteH, TRUE);
		I2C_MSTR_get(&curByteL, FALSE);
		I2C_MSTR_stop();

		if (curByteH == 0x00 && (curByteL == 0x00 || curByteL == 0x01))
			curByteL = 0x02; // Invalid write area, reserve first two bytes for curByte.
		else if (curByteH > 0x39) // EEPROM is full....
			CEENBoT_AT24C128_reset();

		for (i = 0; i < length; i++) {
			while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
				;
			I2C_MSTR_send(curByteH);
			I2C_MSTR_send(curByteL);
			I2C_MSTR_send(input[i]);
			I2C_MSTR_stop();

			if (curByteL == 0xFF) {
				if (curByteH == 0x39) {
					CEENBoT_AT24C128_reset();
				} else {
					curByteL = 0x00;
					curByteH++;
				}
			} else {
				curByteL++;
			}
		}

		while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
			;
		I2C_MSTR_send(curByteH);
		I2C_MSTR_send(curByteL);
		I2C_MSTR_send('*');
		I2C_MSTR_stop();
		if (curByteL == 0xFF) {
			curByteL = 0x00;
			curByteH++;
		} else {
			curByteL++;
		}
		while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
			;
		I2C_MSTR_send_multiple(MemLocationH, 2);
		I2C_MSTR_send(curByteH);
		I2C_MSTR_stop();
		while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
			;
		I2C_MSTR_send_multiple(MemLocationL, 2);
		I2C_MSTR_send(curByteL);
		I2C_MSTR_stop();
	}
}

/**
Function: CEENBoT_AT24C128_read
Arguments:
 	 location	- A STORE_LOCATION structure with the desired location bytes to read from.
Return: Single character value located that the read location. 0xFF is used to indicate error (I2C isn't open).
Detail: This will take the location listed in the STORE_LOCATION structure (location.high and location.low), read the appropriate character and return it for the programmers use.
Members on STORE_LOCATION:
	low		- a character value to indicate the LSB of the storage location.
	high	- a character value to indicate the MSB of the storage location.
Example usage:
#include "CEENBoT_AT24C128_EEPROM.h"
..
	STORE_LOCATION loc;
...
	loc.low = 0x03;
	loc.high = 0x00;
...
	LCD_printf("Loc: %02x%02x : %c\n", loc.high, loc.low, CEENBoT_AT24C128_read(loc));
...
 */
unsigned char CEENBoT_AT24C128_read(STORE_LOCATION location) {
	unsigned char rval = 0xFF;
	if (SYS_get_state(SUBSYS_I2C) == SUBSYS_OPEN) {
		I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT);
		I2C_MSTR_send(location.high);
		I2C_MSTR_send(location.low);
		I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MR);
		I2C_MSTR_get(&rval, FALSE);
		I2C_MSTR_stop();
	}
	return rval;
}

/**
Function: CEENBoT_AT24C128_readAll
Details: This function will spit out the entire contents of the EEPROM into UART0 (J11 on the bot). If the UART isn't configured on calling this function, it will automatically configure it for the user at 115200 baud
 */
void CEENBoT_AT24C128_readAll() {
	unsigned char buf = 0xFF;
	unsigned int i = 2;
	if (SYS_get_state(SUBSYS_UART0) == SUBSYS_CLOSED) {
		UART_open(UART_UART0);
		UART_configure(UART_UART0, UART_8DBITS, UART_1SBIT, UART_NO_PARITY,
				115200);
		UART_set_TX_state(UART_UART0, UART_ENABLE);
	}
	if (SYS_get_state(SUBSYS_I2C) == SUBSYS_OPEN)  // NOTE: No i2c, no STORE....
	{
		I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT);
		I2C_MSTR_send(0x00);
		I2C_MSTR_send(0x00);
		I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MR);
		I2C_MSTR_get(&curByteH, TRUE);
		I2C_MSTR_get(&curByteL, TRUE);
		UART0_printf("Starting log output:\n");
		while (1) {
			if (i >> 8 > curByteH)
				break;
			if (((i >> 8) == curByteH) && ((i & 0x00ff) >= curByteL))
				break;
			I2C_MSTR_get(&buf, TRUE);
			if (buf == '*')
				UART0_printf("\n");
			else
				UART0_printf("%c", buf);
			i++;
		}
		I2C_MSTR_get(&buf, FALSE);
		I2C_MSTR_stop();
	}
}
/**
Function: CEENBoT_AT24C128_reset
Details: This will reset the current storage position (the first two bytes) back to the beginning.
NOTE: This is not a secure delete, data can still be read if not overwritten.
 */
void CEENBoT_AT24C128_reset() {
	if (SYS_get_state(SUBSYS_I2C) == SUBSYS_OPEN) {
		I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT);
		I2C_MSTR_send(0x00);
		I2C_MSTR_send(0x00);
		I2C_MSTR_send(0x00);
		I2C_MSTR_stop();
		while (I2C_MSTR_start(EEPROM_ADDR, I2C_MODE_MT) != I2C_STAT_OK)
			;
		I2C_MSTR_send(0x00);
		I2C_MSTR_send(0x01);
		I2C_MSTR_send(0x02); //Resetting current log location.
		I2C_MSTR_stop();
		curByteL = 0x02;
		curByteH = 0x00;
	}
}
